# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .configuration import Configuration, ConfigurationPurchaseMethod
from .purchase import Purchase


def register():
    Pool.register(
        Configuration,
        ConfigurationPurchaseMethod,
        Purchase,
        module='purchase_shipment_method', type_='model')
