# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class PurchaseShipmentMethodTestCase(ModuleTestCase):
    """Test Purchase Shipment Method module"""
    module = 'purchase_shipment_method'


del ModuleTestCase
